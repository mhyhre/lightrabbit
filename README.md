# Project Light Rabbit (A marine story) #

Copyright (C) 2013-2015 Andrey Tulyakov
@mail: mhyhre@gmail.com

This work is licensed under a Creative Commons 
Attribution-NonCommercial-NoDerivs 3.0 Unported License.
You may obtain a copy of the License at

	http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode

Google play link:
[https://play.google.com/store/apps/details?id=mhyhre.lightrabbit](https://play.google.com/store/apps/details?id=mhyhre.lightrabbit)